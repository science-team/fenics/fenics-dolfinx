Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DOLFIN
Upstream-Contact: fenics@fenicsproject.org
 https://fenicsproject.org/community/
Source: https://github.com/FEniCS/dolfinx

Files: *
Copyright: 2001-2021, Authors/contributors in alphabetical order:
  Ido Akkerman          <I.Akkerman@tudelft.nl>             (-)
  Martin Sandve Alnæs   <martinal@simula.no>                (C)
  Igor Baratta          <ia397@cam.ac.uk>                   (-)
  Fredrik Bengzon       <bengzon@math.chalmers.se>          (-)
  Aslak Bergersen       <aslak.bergersen@gmail.com>         (C)
  Jan Blechta           <blechta@karlin.mff.cuni.cz>        (C)
  Rolv Erlend Bredesen  <rolv@simula.no>                    (C)
  Jed Brown             <fenics@59A2.org>                   (C)
  Solveig Bruvoll       <solveio@ifi.uio.no>                (C)
  Jørgen Dokken         <dokken92@gmail.com>                (-)
  Niklas Ericsson       <nen@math.chalmers.se>              (-)
  Patrick Farrell       <patrick.farrell06@imperial.ac.uk>  (C)
  Georgios Foufas       <foufas@math.chalmers.se>           (C)
  Tom Gustafsson        <tom.gustafsson@aalto.fi>           (C)
  Joachim B Haga        <jobh@broadpark.no>                 (C)
  Johan Hake            <hake@simula.no>                    (C)
  Jack S. Hale          <j.hale09@imperial.ac.uk>           (C)
  Rasmus Hemph          <PDE project course 2001/2002>      (-)
  David Heintz          <david.heintz@comhem.se>            (-)
  Johan Hoffman         <hoffman@csc.kth.se>                (C)
  Par Ingelstrom        <pi@elmagn.chalmers.se>             (-)
  Anders E. Johansen    <andersej@math.uio.no>              (C)
  Johan Jansson         <johanjan@math.chalmers.se>         (C)
  Niclas Jansson        <njansson@kth.se>                   (C)
  Alexander Jarosch     <alexanj@hi.is>                     (C)
  Kristen Kaasbjerg     <cosby@fys.ku.dk>                   (C)
  Benjamin Kehlet       <benjamik@simula.no>                (C)
  Arve Knudsen          <arvenk@simula.no>                  (C)
  Karin Kraft           <kakr@math.chalmers.se>             (-)
  Aleksandra Krusper    <PDE project course 2001/2002>      (-)
  Evan Lezar            <mail@evanlezar.com>                (C)
  Tianyi Li             <tianyi.li@polytechnique.edu>       (C)
  Matthias Liertzer     <matthias.liertzer@tuwien.ac.at>    (C)
  Dag Lindbo            <dag@csc.kth.se>                    (C)
  Glenn Terje Lines     <glennli@simula.no>                 (C)
  Anders Logg           <logg@simula.no>                    (C)
  Nuno Lopes            <ndl@ptmat.fc.ul.pt>                (C)
  Kent-Andre Mardal     <kent-and@simula.no>                (C)
  Andreas Mark          <f00anma@dd.chalmers.se>            (-)
  Andre Massing         <massing@simula.no>                 (C)
  Lawrence Mitchell     <lawrence.mitchell@imperial.ac.uk>  (C)
  Marco Morandini       <morandini@aero.polimi.it>          (C)
  Mikael Mortensen      <mikael.mortensen@gmail.com>        (C)
  Corrado Maurini       <cmaurini@gmail.com>                (C)
  Pablo De Napoli       <pdenapo@gmail.com>                 (-)
  Harish Narayanan      <hnarayanan@gmail.com>              (C)
  Andreas Nilsson       <f99anni@dd.chalmers.se>            (-)
  Minh Do-Quang         <minh@mech.kth.se>                  (-)
  Chris Richardson      <chris@bpi.cam.ac.uk>               (C)
  Johannes Ring         <johannr@simula.no>                 (C)
  Marie E. Rognes       <meg@simula.no>                     (C)
  John Rudge            <jfr23@cam.ac.uk>                   (-)
  Bartosz Sawicki       <sawickib@iem.pw.edu.pl>            (C)
  Nico Schlömer         <nico.schloemer@gmail.com>          (C)
  Matthew Scroggs       <fenics@mscroggs.co.uk>             (-)
  Kristoffer Selim      <selim@simula.no>                   (C)
  Angelo Simone         <a.simone@tudelft.nl>               (C)
  Ola Skavhaug          <skavhaug@simula.no>                (C)
  Thomas Svedberg       <thsv@am.chalmers.se>               (-)
  Erik Svensson         <eriksv@math.chalmers.se>           (C)
  Harald Svensson       <harald.s@home.se>                  (-)
  Andy Terrel           <aterrel@uchicago.edu>              (C)
  Jim Tilander          <jt@dd.chalmers.se>                 (C)
  Fredrik Valdmanis     <fredrik@valdmanis.com>             (C)
  Magnus Vikstrøm       <gustavv@ifi.uio.no>                (C)
  Walter Villanueva     <PDE project course 2001/2002>      (-)
  Shawn Walker          <walker@cims.nyu.edu>               (C)
  Garth N. Wells        <gnw20@cam.ac.uk>                   (C)
  Ilmar Wilbers         <ilmarw@simula.dot.no>              (C)
  Cian Wilson           <cwilson@ldeo.columbia.edu>         (C)
  Ivan Yashchuk         <ivan.yashchuk@aalto.fi>            (C)
  Michele Zaffalon      <michele.zaffalon@gmail.com>        (C)
  Åsmund Ødegård        <aasmund@simula.no>                 (C)
  Kristian Ølgaard      <kbo@civil.aau.dk>                  (C)
License: LGPL-3+
Comment:
 (C) = copyright form signed
 (-) = minor change, copyright form not signed

Files: debian/*
Copyright: 2008-2017, Johannes Ring <johannr@simula.no>
           2017-2018  Drew Parsons <dparsons@debian.org>
License: GPL-2+

Files: cpp/cmake/modules/FindKaHIP.cmake
Copyright:   2019 Igor A. Baratta
License: BSD-2-clause

Files: cpp/cmake/modules/FindParMETIS.cmake
       cpp/cmake/modules/FindSCOTCH.cmake
Copyright: 2010-2020, Garth N. Wells,
                         Anders Logg,
                      Johannes Ring
License: BSD-2-clause

Files: cpp/cmake/modules/FindUFCx.cmake
Copyright: 2010-2021, Johannes Ring and Garth N. Wells
License: BSD-2-clause

License: LGPL-3+
 This software is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 This software is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this software. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU Lesser General Public
 License, version 3 can be found in the file
 '/usr/share/common-licenses/LGPL-3'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: Boost-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This software is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This software is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this software. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public
 License, version 2 can be found in the file
 '/usr/share/common-licenses/GPL-2'.

